package br.com.fatec;

import br.com.fatec.view.InitialScreen;

public class VetClinic {

    public static void main(String[] args) {
        
        java.awt.EventQueue.invokeLater(() -> {
            new InitialScreen().setVisible(true);
        });
    }
    
}
