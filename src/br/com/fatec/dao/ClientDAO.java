package br.com.fatec.dao;

import br.com.fatec.model.Client;
import br.com.fatec.utils.MYSQLConnection;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientDAO {

    private final static String DELETE = "DELETE FROM tb_client WHERE id=?";
    private final static String FIND_ALL = "SELECT * FROM tb_client";
    private final static String FIND_BY_ID = "SELECT * FROM tb_client WHERE id=?";
    private final static String FIND_BY_NAME = "SELECT * FROM user WHERE name=?";
    private final static String INSERT = "INSERT INTO tb_client(name, documentId, gender, address, birth) VALUES(?, ?, ?, ?, ?)";
    private final static String UPDATE = "UPDATE tb_client SET name=?, documentId=?, gender=?, address=?, birth=? WHERE id=?";
    private final static Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());

    public List<Client> findAll() {

        Connection conn = null;
        PreparedStatement stmt = null;
        List<Client> clients = new ArrayList<>();

        try {
            conn = MYSQLConnection.getConnection();
            stmt = conn.prepareStatement(FIND_ALL);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Client client = new Client();
                client.setId(rs.getLong("id"));
                client.setName(rs.getString("name"));
                client.setDocumentId(rs.getString("documentId"));
                client.setGender(rs.getString("gender"));
                client.setAddress(rs.getString("address"));
                if (rs.getDate("birth") != null) {
                    client.setBirth(rs.getDate("birth").toLocalDate());
                }

                clients.add(client);
            }

            LOGGER.log(Level.INFO, "Everything ok! Found: {0} clients!", clients.size());
            return clients;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            MYSQLConnection.close(stmt);
            MYSQLConnection.close(conn);
        }
    }

    public Client findById(Long id) {

        Connection conn = null;
        PreparedStatement stmt = null;
        Client client = null;

        try {
            conn = MYSQLConnection.getConnection();
            stmt = conn.prepareStatement(FIND_BY_ID);
            stmt.setLong(1, id);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                client = new Client();
                client.setId(rs.getLong("id"));
                client.setName(rs.getString("name"));
                client.setDocumentId(rs.getString("documentId"));
                client.setGender(rs.getString("gender"));
                client.setAddress(rs.getString("address"));
                if (rs.getDate("birth") != null) {
                    client.setBirth(rs.getDate("birth").toLocalDate());
                }
            }

            LOGGER.log(Level.INFO, "Everything ok! Found the client {0}", client.getName());
            return client;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            MYSQLConnection.close(stmt);
            MYSQLConnection.close(conn);
        }

    }

    public int insert(Client client) {

        Connection conn = null;
        PreparedStatement stmt = null;

        try {

            conn = MYSQLConnection.getConnection();
            stmt = conn.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, client.getName());
            stmt.setString(2, client.getDocumentId());
            stmt.setString(3, client.getGender());
            stmt.setString(4, client.getAddress());
            if (client.getBirth() != null) {
                stmt.setDate(5, Date.valueOf(client.getBirth()));
            } else {
                stmt.setDate(5, null);
            }

            int result = stmt.executeUpdate();

            return result;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            MYSQLConnection.close(stmt);
            MYSQLConnection.close(conn);
        }

    }

    public int update(Client client) {

        Connection conn = null;
        PreparedStatement stmt = null;

        try {

            conn = MYSQLConnection.getConnection();
            stmt = conn.prepareStatement(UPDATE);
            stmt.setString(1, client.getName());
            stmt.setString(2, client.getDocumentId());
            stmt.setString(3, client.getGender());
            stmt.setString(4, client.getAddress());

            if (client.getBirth() != null) {
                stmt.setDate(5, Date.valueOf(client.getBirth()));
            } else {
                stmt.setDate(5, null);
            }

            stmt.setLong(6, client.getId());

            return stmt.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            MYSQLConnection.close(stmt);
            MYSQLConnection.close(conn);
        }
    }

    public int delete(Long id) {
        
        Connection conn = null;
        PreparedStatement stmt = null;

        try {

            conn = MYSQLConnection.getConnection();
            stmt = conn.prepareStatement(DELETE);
            stmt.setLong(1, id);

            return stmt.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            MYSQLConnection.close(stmt);
            MYSQLConnection.close(conn);
        }

    }

}
