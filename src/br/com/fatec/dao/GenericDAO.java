/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fatec.dao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

public abstract class GenericDAO<T> {

    private static Logger LOGGER = Logger.getLogger(GenericDAO.class.getName());

    private Set<T> objects = new HashSet<T>();

    public boolean add(T t) {
        if (t != null && !objects.contains(t)) {
            if (objects.add(t)) {
                LOGGER.info("Added new " + t.toString());
                return true;
            }
        }

        return false;
    }

    public boolean update(int index, T t) {
        if (index >= 0 && t != null) {
            T obj = getByIndex(index);
            if (obj != null) {
                if (objects.remove(obj) && objects.add(t)) {
                    LOGGER.info("Updated " + obj.toString() + " to " + t.toString());
                    return true;
                }
            }
        }
        return false;
    }

    public boolean remove(T t) {
        if (t != null && objects.contains(t)) {
            if (objects.remove(t)) {
                LOGGER.info("Removed " + t.toString());
                return true;
            }
        }

        return false;
    }

    public T getByIndex(int index) {
        if (index >= 0 && index < objects.size()) {
            return new ArrayList<T>(objects).get(index);
        }

        return null;
    }

    public List<T> getAll() {
        return new ArrayList<T>(objects);
    }
    
}