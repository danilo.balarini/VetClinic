package br.com.fatec.dao;

import br.com.fatec.model.Client;
import br.com.fatec.model.Pet;
import br.com.fatec.utils.MYSQLConnection;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PetDAO {

    private final static String DELETE = "DELETE FROM tb_pet WHERE id=?";
    private final static String FIND_ALL = "SELECT * FROM tb_pet";
    private final static String FIND_BY_ID = "SELECT * FROM tb_pet WHERE id=?";
    private final static String FIND_BY_NAME = "SELECT * FROM tb_pet WHERE name=?";
    private final static String INSERT = "INSERT INTO tb_pet(name, animal, breed, gender, color, birth, owner) VALUES(?, ?, ?, ?, ?, ?, ?)";
    private final static String UPDATE = "UPDATE tb_pet SET name=?, animal=?, breed=?, gender=?, color=?, birth=?, owner=? WHERE id=?";
    private final static Logger LOGGER = Logger.getLogger(PetDAO.class.getName());

    public List<Pet> findAll() {

        Connection conn = null;
        PreparedStatement stmt = null;
        List<Pet> pets = new ArrayList<>();

        try {
            conn = MYSQLConnection.getConnection();
            stmt = conn.prepareStatement(FIND_ALL);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Pet pet = new Pet();
                pet.setId(rs.getLong("id"));
                pet.setName(rs.getString("name"));
                pet.setAnimal(rs.getString("animal"));
                pet.setBreed(rs.getString("breed"));
                pet.setGender(rs.getString("gender"));
                pet.setColor(rs.getString("color"));
                if (rs.getDate("birth") != null) {
                    pet.setBirth(rs.getDate("birth").toLocalDate());
                }
                pet.setOwner(rs.getLong("owner"));

                pets.add(pet);
            }

            LOGGER.log(Level.INFO, "Everything ok! Found: {0} clients!", pets.size());
            return pets;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            MYSQLConnection.close(stmt);
            MYSQLConnection.close(conn);
        }
    }

    public Pet findById(Long id) {

        Connection conn = null;
        PreparedStatement stmt = null;
        Pet pet = null;

        try {
            conn = MYSQLConnection.getConnection();
            stmt = conn.prepareStatement(FIND_BY_ID);
            stmt.setLong(1, id);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                pet = new Pet();
                pet.setId(rs.getLong("id"));
                pet.setName(rs.getString("name"));
                pet.setAnimal(rs.getString("animal"));
                pet.setBreed(rs.getString("breed"));
                pet.setGender(rs.getString("gender"));
                pet.setColor(rs.getString("color"));
                if (rs.getDate("birth") != null) {
                    pet.setBirth(rs.getDate("birth").toLocalDate());
                }
                pet.setOwner(rs.getLong("owner"));
            }

            LOGGER.log(Level.INFO, "Everything ok! Found the pet {0}", pet.getName());
            return pet;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            MYSQLConnection.close(stmt);
            MYSQLConnection.close(conn);
        }

    }

    public int insert(Pet pet) {

        Connection conn = null;
        PreparedStatement stmt = null;

        try {

            conn = MYSQLConnection.getConnection();
            stmt = conn.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, pet.getName());
            stmt.setString(2, pet.getAnimal());
            stmt.setString(3, pet.getBreed());
            stmt.setString(4, pet.getGender());
            stmt.setString(5, pet.getColor());
            if (pet.getBirth() != null) {
                stmt.setDate(6, Date.valueOf(pet.getBirth()));
            } else {
                stmt.setDate(6, null);
            }
            stmt.setLong(7, pet.getOwner());

            int result = stmt.executeUpdate();

            return result;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            MYSQLConnection.close(stmt);
            MYSQLConnection.close(conn);
        }

    }

    public int update(Pet pet) {

        Connection conn = null;
        PreparedStatement stmt = null;

        try {

            conn = MYSQLConnection.getConnection();
            stmt = conn.prepareStatement(UPDATE);
            
            stmt.setString(1, pet.getName());
            stmt.setString(2, pet.getAnimal());
            stmt.setString(3, pet.getBreed());
            stmt.setString(4, pet.getGender());
            stmt.setString(5, pet.getColor());
            if (pet.getBirth() != null) {
                stmt.setDate(6, Date.valueOf(pet.getBirth()));
            } else {
                stmt.setDate(6, null);
            }
            stmt.setLong(7, pet.getOwner());
            stmt.setLong(8, pet.getId());

            return stmt.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            MYSQLConnection.close(stmt);
            MYSQLConnection.close(conn);
        }
    }

    public int delete(Long id) {
        
        Connection conn = null;
        PreparedStatement stmt = null;

        try {

            conn = MYSQLConnection.getConnection();
            stmt = conn.prepareStatement(DELETE);
            stmt.setLong(1, id);

            return stmt.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            MYSQLConnection.close(stmt);
            MYSQLConnection.close(conn);
        }

    }

}
