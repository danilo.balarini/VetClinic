package br.com.fatec.dao;

import br.com.fatec.model.Pet;
import br.com.fatec.model.Schedule;
import br.com.fatec.utils.MYSQLConnection;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ScheduleDAO {

    private final static String DELETE = "DELETE FROM tb_schedule WHERE id=?";
    private final static String FIND_ALL = "SELECT * FROM tb_schedule";
    private final static String FIND_BY_ID = "SELECT * FROM tb_schedule WHERE id=?";
    private final static String INSERT = "INSERT INTO tb_schedule(description, start, finish, ownerId, petId) VALUES(?, ?, ?, ?, ?)";
    private final static String UPDATE = "UPDATE tb_schedule SET description=?, start=?, finish=?, ownerId=?, petId=? WHERE id=?";
    private final static Logger LOGGER = Logger.getLogger(ScheduleDAO.class.getName());

    public List<Schedule> findAll() {

        Connection conn = null;
        PreparedStatement stmt = null;
        List<Schedule> schedules = new ArrayList<>();

        try {
            conn = MYSQLConnection.getConnection();
            stmt = conn.prepareStatement(FIND_ALL);
            ResultSet rs = stmt.executeQuery();
            
            while (rs.next()) {
                Schedule schedule = new Schedule();
                schedule.setId(rs.getLong("id"));
                schedule.setDescription(rs.getString("description"));
                schedule.setStart(rs.getTimestamp("start").toLocalDateTime());
                schedule.setFinish(rs.getTimestamp("finish").toLocalDateTime());
                schedule.setPetId(rs.getLong("petId"));

                schedules.add(schedule);
            }

            LOGGER.log(Level.INFO, "Everything ok! Found: {0} clients!", schedules.size());
            return schedules;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            MYSQLConnection.close(stmt);
            MYSQLConnection.close(conn);
        }
    }

    public Schedule findById(Long id) {

        Connection conn = null;
        PreparedStatement stmt = null;
        Schedule schedule = null;

        try {
            conn = MYSQLConnection.getConnection();
            stmt = conn.prepareStatement(FIND_BY_ID);
            stmt.setLong(1, id);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                schedule = new Schedule();
                schedule.setId(rs.getLong("id"));
                schedule.setDescription(rs.getString("description"));
                schedule.setStart(rs.getTimestamp("start").toLocalDateTime());
                schedule.setFinish(rs.getTimestamp("finish").toLocalDateTime());
                schedule.setPetId(rs.getLong("petId"));               
            }

            LOGGER.log(Level.INFO, "Everything ok! Found the schedule {0}", schedule.getDescription());
            return schedule;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            MYSQLConnection.close(stmt);
            MYSQLConnection.close(conn);
        }

    }

    public int insert(Schedule schedule) {

        Connection conn = null;
        PreparedStatement stmt = null;

        try {

            conn = MYSQLConnection.getConnection();
            stmt = conn.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, schedule.getDescription());

            int result = stmt.executeUpdate();

            return result;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            MYSQLConnection.close(stmt);
            MYSQLConnection.close(conn);
        }

    }

    public int update(Pet pet) {

        Connection conn = null;
        PreparedStatement stmt = null;

        try {

            conn = MYSQLConnection.getConnection();
            stmt = conn.prepareStatement(UPDATE);

            stmt.setString(1, pet.getName());
            stmt.setString(2, pet.getAnimal());
            stmt.setString(3, pet.getBreed());
            stmt.setString(4, pet.getGender());
            stmt.setString(5, pet.getColor());
            if (pet.getBirth() != null) {
                stmt.setDate(6, Date.valueOf(pet.getBirth()));
            } else {
                stmt.setDate(6, null);
            }
            stmt.setLong(7, pet.getOwner());
            stmt.setLong(8, pet.getId());

            return stmt.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            MYSQLConnection.close(stmt);
            MYSQLConnection.close(conn);
        }
    }

    public int delete(Long id) {

        Connection conn = null;
        PreparedStatement stmt = null;

        try {

            conn = MYSQLConnection.getConnection();
            stmt = conn.prepareStatement(DELETE);
            stmt.setLong(1, id);

            return stmt.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            MYSQLConnection.close(stmt);
            MYSQLConnection.close(conn);
        }

    }

}
