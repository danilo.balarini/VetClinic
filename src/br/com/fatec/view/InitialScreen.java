package br.com.fatec.view;

import br.com.fatec.dao.ClientDAO;
import br.com.fatec.model.Client;
import java.awt.event.ActionEvent;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.Year;
import java.util.logging.Level;
import java.util.logging.Logger;

public class InitialScreen extends javax.swing.JFrame {

    private final static Logger LOGGER = Logger.getLogger(InitialScreen.class.getName());

    public InitialScreen() {

        initComponents();

        jButton1.addActionListener((ActionEvent e) -> {
            LOGGER.log(Level.INFO, "Evento: {0}", e.paramString());
            new ClientDAO().findAll();
        });

        jButton2.addActionListener((ActionEvent e) -> {
            LOGGER.log(Level.INFO, "Evento: {0}", e.paramString());

            Client client = new Client();
            client.setName(jTextField1.getText());
            client.setDocumentId(jTextField2.getText());

            if (jComboBox1.getSelectedItem().equals("Masculino")) {
                client.setGender("M");
            } else {
                client.setGender("F");
            }

            client.setAddress(jTextField3.getText());

            LocalDate localdate = null;
            if (jFormattedTextField1.getText().length() != 10) {
                LOGGER.severe("ERRO - A data não está formatada corretamente");
            } else {
                String[] birth = jFormattedTextField1.getText().split("/");
                localdate = Year.of(new Integer(birth[2])).atMonth(new Integer(birth[1])).atDay(new Integer(birth[0]));
            }

            client.setBirth(localdate);

            LOGGER.log(Level.INFO, "{0}", jFormattedTextField1.getText());

            new ClientDAO().insert(client);
        });

        // ## Find By ID
        btnUpdateFindById.addActionListener((ActionEvent e) -> {
            LOGGER.log(Level.INFO, "Evento: {0}", e.paramString());

            Client client = new ClientDAO().findById(new Long(txtUpdateId.getText()));

            if (client == null) {
                LOGGER.log(Level.INFO, "Não existe cliente para este id: {0}", txtUpdateId.getText());
            } else {
                enableFieldsUpdate(client);
            }

        });

        // ## Update
        btnUpdate.addActionListener((ActionEvent e) -> {
            LOGGER.log(Level.INFO, "Evento: {0}", e.paramString());

            Client client = new Client();
            client.setId(new Long(txtUpdateId.getText()));
            client.setName(txtUpdateName.getText());
            client.setDocumentId(txtUpdateDocumentId.getText());

            if (jComboBox1.getSelectedItem().equals("Masculino")) {
                client.setGender("M");
            } else {
                client.setGender("F");
            }

            client.setAddress(txtUpdateAddress.getText());

            LocalDate localdate = null;
            if (txtUpdateBirth.getText() != null && !(txtUpdateBirth.getText().equals(""))) {
                if (txtUpdateBirth.getText().length() != 10) {
                    LOGGER.severe("ERRO - A data não está formatada corretamente");
                } else {
                    String[] birth = txtUpdateBirth.getText().split("/");
                    localdate = Year.of(new Integer(birth[2])).atMonth(new Integer(birth[1])).atDay(new Integer(birth[0]));
                }
            }

            client.setBirth(localdate);

            new ClientDAO().update(client);
            disableFieldsUpdate();

        });
        
        // ## Cancel update/delete
        btnUpdateCancel.addActionListener((ActionEvent e) -> {
            LOGGER.log(Level.INFO, "Evento: {0}", e.paramString());
            disableFieldsUpdate();
        });

        // ## delete
        btnDelete.addActionListener((ActionEvent e) -> {
            LOGGER.log(Level.INFO, "Evento: {0}", e.paramString());
            new ClientDAO().delete(new Long(txtUpdateId.getText()));
            disableFieldsUpdate();
        });

    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jTextField3 = new javax.swing.JTextField();
        jFormattedTextField1 = new javax.swing.JFormattedTextField();
        try {
            javax.swing.text.MaskFormatter data= new javax.swing.text.MaskFormatter("##/##/####");
            jFormattedTextField1 = new javax.swing.JFormattedTextField(data);
        } catch (ParseException ex) {
            Logger.getLogger(InitialScreen.class.getName()).log(Level.SEVERE, null, ex);
        }
        jButton2 = new javax.swing.JButton();
        jComboBox1 = new javax.swing.JComboBox<>();
        txtUpdateName = new javax.swing.JTextField();
        txtUpdateId = new javax.swing.JTextField();
        txtUpdateDocumentId = new javax.swing.JTextField();
        comboUpdateGender = new javax.swing.JComboBox<>();
        txtUpdateAddress = new javax.swing.JTextField();
        txtUpdateBirth = new javax.swing.JFormattedTextField();
        try {
            javax.swing.text.MaskFormatter data= new javax.swing.text.MaskFormatter("##/##/####");
            txtUpdateBirth = new javax.swing.JFormattedTextField(data);
        } catch (ParseException ex) {
            Logger.getLogger(InitialScreen.class.getName()).log(Level.SEVERE, null, ex);
        }
        btnUpdate = new javax.swing.JButton();
        btnUpdateFindById = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnUpdateCancel = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem4 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setSize(new java.awt.Dimension(100, 100));

        jButton1.setText("jButton1");

        jTextField1.setName("txtName"); // NOI18N

        jTextField2.setName("txtDocument"); // NOI18N

        jTextField3.setName("txtAddress"); // NOI18N

        jFormattedTextField1.setName("txtBirth"); // NOI18N

        jButton2.setText("Inserir");
        jButton2.setName("btnInsert"); // NOI18N

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Masculino", "Feminino" }));
        jComboBox1.setName("comboGender"); // NOI18N
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        txtUpdateName.setEnabled(false);
        txtUpdateName.setFocusable(false);

        txtUpdateDocumentId.setEnabled(false);
        txtUpdateDocumentId.setFocusable(false);

        comboUpdateGender.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Masculino", "Feminino" }));
        comboUpdateGender.setEnabled(false);
        comboUpdateGender.setFocusable(false);
        comboUpdateGender.setName("comboGender"); // NOI18N
        comboUpdateGender.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboUpdateGenderActionPerformed(evt);
            }
        });

        txtUpdateAddress.setEnabled(false);
        txtUpdateAddress.setFocusable(false);

        txtUpdateBirth.setEnabled(false);
        txtUpdateBirth.setFocusable(false);
        txtUpdateBirth.setName("txtBirth"); // NOI18N

        btnUpdate.setText("Alterar");
        btnUpdate.setEnabled(false);
        btnUpdate.setFocusable(false);
        btnUpdate.setName("btnInsert"); // NOI18N

        btnUpdateFindById.setText("Pesquisar");

        btnDelete.setText("Remover");
        btnDelete.setEnabled(false);
        btnDelete.setFocusable(false);

        btnUpdateCancel.setText("Cancelar");
        btnUpdateCancel.setEnabled(false);
        btnUpdateCancel.setFocusable(false);

        jMenu1.setText("Gerenciar");

        jMenuItem1.setText("Clientes");
        jMenu1.add(jMenuItem1);

        jMenuItem2.setText("Animais");
        jMenu1.add(jMenuItem2);

        jMenuItem3.setText("Exames");
        jMenu1.add(jMenuItem3);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Relatórios");

        jMenuItem4.setText("Clientes");
        jMenu2.add(jMenuItem4);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jButton1)
                            .addGap(63, 63, 63)
                            .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(136, 136, 136)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton2)
                            .addComponent(jFormattedTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(87, 87, 87)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(txtUpdateId, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnUpdateFindById))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(txtUpdateName, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnUpdateCancel))
                    .addComponent(txtUpdateDocumentId, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboUpdateGender, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtUpdateAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtUpdateBirth, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnUpdate)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnDelete)))
                .addContainerGap(474, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtUpdateId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnUpdateFindById))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtUpdateName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnUpdateCancel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtUpdateDocumentId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboUpdateGender, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtUpdateAddress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jFormattedTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtUpdateBirth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2)
                    .addComponent(btnUpdate)
                    .addComponent(btnDelete))
                .addContainerGap(266, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void comboUpdateGenderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboUpdateGenderActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboUpdateGenderActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(InitialScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(InitialScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(InitialScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(InitialScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new InitialScreen().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JButton btnUpdateCancel;
    private javax.swing.JButton btnUpdateFindById;
    private javax.swing.JComboBox<String> comboUpdateGender;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JFormattedTextField jFormattedTextField1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField txtUpdateAddress;
    private javax.swing.JFormattedTextField txtUpdateBirth;
    private javax.swing.JTextField txtUpdateDocumentId;
    private javax.swing.JTextField txtUpdateId;
    private javax.swing.JTextField txtUpdateName;
    // End of variables declaration//GEN-END:variables

    private void enableFieldsUpdate(Client client) {

        btnUpdateCancel.setFocusable(true);
        btnUpdateCancel.setEnabled(true);
        
        txtUpdateId.setFocusable(false);
        txtUpdateId.setEnabled(false);

        txtUpdateName.setFocusable(true);
        txtUpdateName.setEnabled(true);
        txtUpdateName.setText(client.getName());

        txtUpdateDocumentId.setFocusable(true);
        txtUpdateDocumentId.setEnabled(true);
        txtUpdateDocumentId.setText(client.getDocumentId());

        comboUpdateGender.setFocusable(true);
        comboUpdateGender.setEnabled(true);
        comboUpdateGender.setSelectedItem(client.getGender());

        txtUpdateAddress.setFocusable(true);
        txtUpdateAddress.setEnabled(true);
        txtUpdateAddress.setText(client.getAddress());

        txtUpdateBirth.setFocusable(true);
        txtUpdateBirth.setEnabled(true);
        if (client.getBirth() != null) {
            String birth = client.getBirth().getDayOfMonth() + "/" + client.getBirth().getMonthValue() + "/" + client.getBirth().getYear();
            txtUpdateBirth.setText(birth);
        }

        btnUpdate.setFocusable(true);
        btnUpdate.setEnabled(true);

        btnDelete.setFocusable(true);
        btnDelete.setEnabled(true);
    }

    private void disableFieldsUpdate() {

        btnUpdateCancel.setFocusable(false);
        btnUpdateCancel.setEnabled(false);
        
        txtUpdateId.setFocusable(true);
        txtUpdateId.setEnabled(true);
        txtUpdateId.setText("");

        txtUpdateName.setFocusable(false);
        txtUpdateName.setEnabled(false);
        txtUpdateName.setText("");

        txtUpdateDocumentId.setFocusable(false);
        txtUpdateDocumentId.setEnabled(false);
        txtUpdateDocumentId.setText("");

        comboUpdateGender.setFocusable(false);
        comboUpdateGender.setEnabled(false);
        comboUpdateGender.setSelectedIndex(0);

        txtUpdateAddress.setFocusable(false);
        txtUpdateAddress.setEnabled(false);
        txtUpdateAddress.setText("");

        txtUpdateBirth.setFocusable(false);
        txtUpdateBirth.setEnabled(false);
        txtUpdateBirth.setText("");

        btnUpdate.setFocusable(false);
        btnUpdate.setEnabled(false);

        btnDelete.setFocusable(false);
        btnDelete.setEnabled(false);
    }

}
